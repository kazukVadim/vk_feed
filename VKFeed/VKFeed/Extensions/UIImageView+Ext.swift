//
//  UIImageView+Ext.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 22.09.21.
//

import UIKit

extension UIImageView {
    
    func imageFromServerURL(_ URLString: String, placeHolder: UIImage? = UIImage(), priceImage: UIImageView? = UIImageView(), label: UILabel? = UILabel()) {
        
        self.image = nil
        priceImage?.isHidden = true
        label?.isHidden = true
        let imageServerUrl = URLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        if let url = URL(string: imageServerUrl) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil {
                    print("ERROR LOADING IMAGES FROM URL: \(String(describing: error))")
                    DispatchQueue.main.async {
                        self.image = placeHolder
                        
                    }
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            self.image = downloadedImage
                            priceImage?.isHidden = false
                            label?.isHidden = false
                        }
                    }
                }
            }).resume()
        }
    }
    
}
