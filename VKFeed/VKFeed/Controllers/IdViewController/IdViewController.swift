//
//  IdViewController.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 22.09.21.
//

import UIKit

class IdViewController: UIViewController {

    @IBOutlet weak var enterIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enterIdTextField.keyboardType = .numberPad
        self.enterIdTextField.text = "203438034"//"203438034"//

    }

    @IBAction func nextButtonAction(_ sender: Any) {

        if let id = self.enterIdTextField.text, !id.isEmpty {
            let feedViewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(identifier: "FeedViewController", creator: { coder -> FeedViewController? in
                FeedViewController(coder: coder, id: id)
            })
            self.navigationController?.pushViewController(feedViewController, animated: true)
        }
        
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        let loginViewController = UIStoryboard(name: "Main", bundle: .main).instantiateViewController(identifier: "LoginViewController")
        self.navigationController?.pushViewController(loginViewController, animated: true)
    }
    
}
