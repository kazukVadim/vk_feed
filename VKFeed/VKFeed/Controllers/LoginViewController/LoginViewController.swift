//
//  ViewController.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 20.09.21.
//

import UIKit
import WebKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = URL(string: "https://oauth.vk.com/authorize?client_id=6746670&scope=offline,friends,docs&redirect_uri=https://oauth.vk.com/blank.html&display=mobile&v=5.131&response_type=token") else { return }
        self.webView.navigationDelegate = self
        self.webView.load(URLRequest(url: url))
        
    }
    
}

extension LoginViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

        // String extension of func
        var token = ""
        let url = navigationAction.request.url?.absoluteString
        
        if url?.range(of: "#access_token") != nil {
            let urlHalf = url?.components(separatedBy: "&").first ?? ""
            token = urlHalf.components(separatedBy: "=").last ?? ""
        }
        
        if !token.isEmpty {
            print(token)
            UserDefaults.standard.set(token, forKey: "savedToken")
            self.navigationController?.popViewController(animated: true)
        }

        decisionHandler(.allow)
    }

}
