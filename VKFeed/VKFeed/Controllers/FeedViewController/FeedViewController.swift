//
//  FeedViewController.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 20.09.21.
//

import UIKit
import Alamofire

class FeedViewController: UIViewController {

    init?(coder: NSCoder, id: String) {
        self.id = id
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    let spinner = UIActivityIndicatorView(style: .medium)
    let token = UserDefaults.standard.object(forKey: "savedToken")
    var feed: [Items] = []
    var user = [User]()
    var id: String


    var postsCount = 5
    private var page = 1
    private var offset: Int {
        postsCount * page
    }

    private var isDataLoading = false
    private var isEndReached = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: FeedCell.identifier, bundle: nil), forCellReuseIdentifier: FeedCell.identifier)
        self.tableView.tableFooterView = self.spinner
        self.spinner.frame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: 44)
        self.tableView.tableFooterView?.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.fetchWallData(initial: true)
        self.fetchPersonalData()
    }
    
    func fetchWallData(initial: Bool) {
        if initial == true {
            self.isEndReached = false
            self.page = 1
        }

        if self.isDataLoading == true || self.isEndReached == true {
            return
        }

        self.isDataLoading = true

        self.spinner.startAnimating()

        Alamofire.AF.request("https://api.vk.com/method/wall.get?", method: .get, parameters: ["owner_id": self.id,
                                                                                               "filter": "all",
                                                                                               "count": self.postsCount,
                                                                                               "offset": self.offset,
                                                                                               "access_token": self.token ?? "",
                                                                                               "v": 5.131]).responseData(completionHandler: { [weak self] (response) in
            // if let error = response.error { show error } else { decode data }

            self?.spinner.stopAnimating()
            self?.isDataLoading = false
            guard let data = response.value else { return }
            do {
                let feed = try JSONDecoder().decode(FeedModel.self, from: data).response

                if feed.items.count == 0 {
                    self?.isEndReached = true
                } else {
                    if initial {
                        self?.feed = feed.items
                    } else {
                        self?.feed.append(contentsOf: feed.items)
                    }

                    self?.tableView.reloadData()

                    self?.page += 1
                }

            } catch {
                //decoding error
                print(error)
            }
        })
        
    }
    
    func fetchPersonalData() {
        print("TOKEN - \(token ?? "")")
        Alamofire.AF.request("https://api.vk.com/method/users.get?", method: .get, parameters: ["user_ids": self.id,
                                                                                                "fields": ["photo_50", "photo_max_orig"],
                                                                                                "access_token": self.token ?? "",
                                                                                                "v": 5.131]).responseData(completionHandler: { (response) in
            guard let data = response.value else { return }
            do {
                let user = try JSONDecoder().decode(UserModel.self, from: data).response
                self.user = user
                self.tableView.reloadData()
                print(user)
            } catch {
                if self.token != nil {
                    self.errorAlert()
                } else {
                    
                }
                print(error)
            }
        })
        
    }
    
    func errorAlert(title: String? = "Not found", message: String? = "No user or user data is private") {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: { _ in
            self.navigationController?.popViewController(animated: true)
            self.view.endEditing(true)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}

extension FeedViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FeedCell.height
    }

}

extension FeedViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // лучше использовать 1 guard
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeedCell.identifier, for:  indexPath as IndexPath) as? FeedCell else { return UITableViewCell(frame: .zero) }
        let wall = self.feed[indexPath.row]
        guard let user = self.user.first else { return UITableViewCell() }
        
        cell.update(wallData: wall, userData: user, screenWidth: self.view.bounds.width)
        return cell
    }
}

extension FeedViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.height && scrollView.contentSize.height > scrollView.frame.height {

            self.fetchWallData(initial: false)
        }
    }
}
