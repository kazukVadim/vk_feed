//
//  FeedCell.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 20.09.21.
//

import UIKit

class FeedCell: UITableViewCell {
    
    static var identifier: String { String(describing: self) }
    static let height: CGFloat = UITableView.automaticDimension
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var repostTitleLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var repostsCountLabel: UILabel!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var imageComntainerStackView: UIStackView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.width / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageComntainerStackView.subviews.forEach({$0.removeFromSuperview()})
        self.avatarImageView.image = nil
        self.titleLabel.text = ""
        self.nameLabel.text = ""
        self.likesCountLabel.text = ""
        self.commentsCountLabel.text = ""
        self.repostsCountLabel.text = ""
        self.viewsCountLabel.text = ""
        self.repostTitleLabel.text = ""
    }
    
    func update(wallData: Items, userData: User, screenWidth: CGFloat) {
        
        self.avatarImageView.imageFromServerURL(userData.photo_50 ?? "")
        self.nameLabel.text = userData.fullName
        self.titleLabel.text = wallData.text
        self.likesCountLabel.text = "\(wallData.likes.count)"
        self.commentsCountLabel.text = "\(wallData.comments.count)"
        self.repostsCountLabel.text = "\(wallData.reposts.count)"
        self.viewsCountLabel.text = "\(wallData.views?.count ?? 0)"
        
        if let attachments = wallData.attachments {
            for attachment in attachments {
                self.imageLoader(attachment: attachment, screenWidth: screenWidth)
            }
        }
        
        if let copyHistorys = wallData.copy_history {
            for copyHistory in copyHistorys {
                guard let attachments = copyHistory.attachments else { return }
                for attachment in attachments {
                    self.imageLoader(attachment: attachment, screenWidth: screenWidth)
                    self.repostTitleLabel.text = copyHistory.text
                }
            }
        }
        
    }
    
    func imageLoader(attachment: Attachments, screenWidth: CGFloat) {
        
        if attachment.type == "photo" {
            guard let photos = attachment.photo?.sizes else { return }
            guard let photo = photos.last else { return }
            self.imageSetup(photoUrl: photo.url, height: photo.height, width: photo.width, screenWidth: screenWidth)
        } else if attachment.type == "video" {
            guard let photos = attachment.video?.image else { return }
            guard let photo = photos.last else { return }
            self.imageSetup(photoUrl: photo.url, height: photo.height, width: photo.width, screenWidth: screenWidth)
        }
        
    }
    
    func imageSetup(photoUrl: String, height: Int, width: Int, screenWidth: CGFloat) {
        
        let postImageView = UIImageView()
        
        let originalHeight = height
        let originalWidth = width
        
        let newWidth = screenWidth
        let newHeight = (CGFloat(originalHeight) / CGFloat(originalWidth)) * CGFloat(newWidth)
        
        postImageView.translatesAutoresizingMaskIntoConstraints = false
        postImageView.widthAnchor.constraint(equalToConstant: CGFloat(newWidth)).isActive = true
        postImageView.heightAnchor.constraint(equalToConstant: CGFloat(newHeight)).isActive = true
        
        postImageView.imageFromServerURL(photoUrl)
        self.imageComntainerStackView.addArrangedSubview(postImageView)
        
    }
    
}
