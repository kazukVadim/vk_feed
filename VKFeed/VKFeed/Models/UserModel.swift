//
//  UserModel.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 22.09.21.
//

import Foundation

struct UserModel: Codable {
    let response: [User]
}

struct User: Codable {
    
    let name: String?
    let first_name: String?
    let last_name: String?
    let photo_max_orig: String?
    let photo_50: String?
    let type: String?
    
    var fullName: String {
        return self.first_name != nil ? (first_name ?? "") + " " + (last_name ?? "") : (self.name ?? "")
    }
    
}
