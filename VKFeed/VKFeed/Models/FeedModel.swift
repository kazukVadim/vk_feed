//
//  FeedModel.swift
//  VKFeed
//
//  Created by Vadim Kazuk on 21.09.21.
//

import Foundation

struct FeedModel: Codable {
    let response: Feed
}

struct Feed: Codable {
    let items: [Items]
}

struct Items: Codable {
    
    let text: String
    let attachments: [Attachments]?
    let comments: Comments
    let date: Int
    let likes: Likes
    let reposts: Reposts
    let views: Views?
    let copy_history: [CopyHistory]?
    
}

struct Attachments: Codable {
    
    let type: String
    let photo: Photo?
    let video: Video?
    
}

struct Photo: Codable {
    let sizes: [Sizes]
}

struct Sizes: Codable {
    
    let height: Int
    let width: Int
    let url: String
    let type: String
    
}

struct Video: Codable {
    let image: [VideoImage]
}

struct VideoImage: Codable {
    
    let height: Int
    let width: Int
    let url: String
    
}

struct Comments: Codable {
    let count: Int
}

struct Likes: Codable {
    let count: Int
}

struct Reposts: Codable {
    let count: Int
}

struct Views: Codable {
    let count: Int
}

struct CopyHistory: Codable {
    
    let text: String
    let owner_id: Int
    let attachments: [Attachments]?
    
}
